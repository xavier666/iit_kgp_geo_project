## CSV File Concatenation

This small script combines multiple csv files into one single file for the IIT Kharagpur Geology Department

## Usage

1. Put all `csv` files in a folder named `data_files`
2. Run the command `python csv_concat`
3. A new `csv` file will be created with the name format `firstID_lastID_file_list.csv`

More will be updated
