import csv
import os

def write_list_to_file(file_name, the_list):
	fpcsv = csv.writer(open(file_name, 'wb'))

	for i in the_list:
		fpcsv.writerow(i)

def main():
	# path of csv files
	path_to_file = './data_files/'

	# geting list of files inside the path
	list_of_files = os.listdir(path_to_file)

	mega_list = []

	for i in list_of_files:

		# checking if extension is csv or not
		if i.split('.')[1] == 'csv' or i.split('.')[1] == 'CSV':

			# getting the file ID
			file_id = i.split('_')[0]

			#print file_id
			
			# creating the full path of the file
			full_path = path_to_file + i

			fpcsv = csv.reader(open(full_path, 'r'))

			# skipping the header
			fpcsv.next()

			# flag variable to add the id
			first_line = 0

			for j in fpcsv:

				if first_line == 0:
					j.append(file_id)
					first_line = 1

				mega_list.append(j)

	# creating the file name of the combined list
	id_file_1 = list_of_files[0].split('_')[0]
	id_file_2 = list_of_files[-1].split('_')[0]

	ml_file_name = '%s_%s_list.csv' % (id_file_1, id_file_2)

	# writing list to file
	write_list_to_file(ml_file_name, mega_list)

	print 'File created! ^_^'

if __name__ == '__main__':
	main()
